"""gatewayBack URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from gatewayBackApp.views import build_ucp_one,build_ucp_two,user_register
from gatewayBackApp.views import sensor_register
from gatewayBackApp.views import user_login,login_two

urlpatterns = [
    path('admin/', admin.site.urls),
    path('build_ucp_one',build_ucp_one),
    path('build_ucp_two',build_ucp_two),
    path('user_register',user_register),
    path('sensor_register',sensor_register),
    path('user_login',user_login),
    path('login_two',login_two),
]
