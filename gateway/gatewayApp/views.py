from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from gatewayBackApp.models import gateway,sen
import json
import simplejson

import requests

import random
import time
import datetime
from hashlib import md5

from Crypto import Random
from Crypto.PublicKey import RSA
import base64
from Crypto.Cipher import PKCS1_v1_5 as PKCS1_cipher
from pyDes import *

import binascii
import socket
import hmac

from Crypto.Util.number import long_to_bytes,bytes_to_long
from pysmx.SM2 import *

# Create your views here.

IDucp = '10001111001100110000111010101001'
len_para = 64
#TCj_haha = ''
#IDj_haha = ''
#UIDj_haha=''

#基本操作


def str_xor(s1, s2):
  s = ""#结果字符串
  l1 = len(s1)
  l2 = len(s2)
  print(l1, l2)
  for i in range(0, min(l1, l2)):#首先将两个字符串中本来存在的数据进行异或
    # print(s1[i], s2[i])
    a = ord(s1[i])#字符串转十进制
    b = ord(s2[i])
    s = s + chr(a^b)#将异或后结果转字符存到结果字符串中
  # print(s)
  for i in range(min(l1,l2),max(l1,l2)):#将某一字符串多出来的部分进行异或操作
    if(l1<l2):
      s = s + chr(ord(s2[i])^255)
    else :
      s = s + chr(ord(s1[i])^255)#和11111111进行异或
  return s



#通信建立阶段
def build():
    pk, sk = generate_keypair()
    with open('sm2_gate_private_key.pem', 'wb')as f:
        f.write(sk)
    with open('sm2_gate_public_key.pem', 'wb')as f:
        f.write(pk)

def get_key(key_file):
    with open(key_file ,'rb') as f:
        data = f.read()
    return data


def build_ucp_one(request):
    TS1_str = request.POST.get('TS1')
    global r1
    r1 = request.POST.get('r1')
    TS1 = float(TS1_str)
    TS_now = time.time()
    if (TS_now-TS1)<15:
        global r2
        r2 = str(bin(random.randint(0, 99999999999999999999))[2:])
        TS2 = time.time()
        build() #sm2秘钥生成
        Kp = get_key('sm2_gate_public_key.pem')#获取sm2公钥
        post_data = {'r2': r2,'Kp':str(Kp),'TS2':TS2}
        response = requests.post('http://localhost:8000/build_user_two', data=post_data)
        return HttpResponse(response)
    else:
        return HttpResponse('fail')

def build_ucp_two(request):
    TS3_str = request.POST.get('TS3')
    ER = eval(request.POST.get('ER'))
    TS3 = float(TS3_str)
    TS_now = time.time()
    if (TS_now-TS3)<15:
        private_key = get_key('sm2_gate_private_key.pem')
        back_text = Decrypt(ER, get_key('sm2_gate_private_key.pem'), len_para)
        res = eval(str(back_text)[2:-1])
        r3 = res["r3"]
        TS3_decode = res["TS3"]
        if (TS3 == TS3_decode):
            global SK
            linshi = str(r1)+str(r2)+str(r3)
            SK = md5(linshi.encode('utf-8')).hexdigest()
            print(SK)
            return HttpResponse('success')
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')

    

#用户注册阶段
#DES
def des_encrypt(s):
    """
    DES 加密
    :param s: 原始字符串
    :return: 加密后字符串，16进制
    """
    secret_key = SK[0:8]
    iv = secret_key
    k = des(secret_key, CBC, iv, pad=None, padmode=PAD_PKCS5)
    en = k.encrypt(s, padmode=PAD_PKCS5)
    return binascii.b2a_hex(en)
 
 
def des_descrypt(s):
    """
    DES 解密
    :param s: 加密后的字符串，16进制
    :return:  解密后的字符串
    """
    secret_key = SK[0:8]
    iv = secret_key
    k = des(secret_key, CBC, iv, pad=None, padmode=PAD_PKCS5)
    de = k.decrypt(binascii.a2b_hex(s), padmode=PAD_PKCS5)
    return de

def user_register(request):
    TS1_str = request.POST.get('TS1')
    EIDi = eval(request.POST.get('EIDi'))
    TS_now = time.time()
    TS1 = float(TS1_str)
    if (TS_now-TS1)<15:
        back_text = des_descrypt(EIDi)
        res = eval(back_text.decode('utf-8'))
        UIDi = res["UIDi"]
        print('------------------')
        print('UIDi:'+UIDi)
        print('------------------')
        TS1_decode = res["TS1"]
        if (TS1 == TS1_decode):
            s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            s.connect(('47.108.195.179',34126))
            s.send(bytes(UIDi.encode('utf-8')))
            print(UIDi.encode('utf-8'))
            PIDi=s.recv(1024)
            testhaha = ''
            for i in PIDi:
                testhaha += '%02x' % i
            s.close()
            print(testhaha)
            PIDi = testhaha
            isRegister = gateway.objects.filter(PIDi = PIDi).exists()
            if isRegister:#在列表中说明用户已经注册过了
                return HttpResponse('has_reg')
            else:
                TS2 = time.time()
                linshi = str(IDucp)+str(PIDi)
                V = md5(linshi.encode('utf-8')).hexdigest()
                first_test = {"TS2":TS2}
                se_test = json.dumps(first_test)
                ACKucp = des_encrypt(se_test)
                myGWN = gateway(PIDi = PIDi,V = V)
                myGWN.save()
                post_data = {'ACKucp': str(ACKucp),'TS2':TS2}
                response = requests.post('http://localhost:8000/register_two', data=post_data)
                return HttpResponse(response)
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')




#功能平台注册阶段
def sensor_register(request):
    TS1 = eval(request.POST.get('TS1'))
    IDj = request.POST.get('IDj')
    TS_now = time.time()
    if (TS_now-TS1)<15:
        Rj = str(bin(random.randint(0, 99999999999999999999))[2:])
        linshi = str(IDj)+str(Rj)
        TCj = md5(linshi.encode('utf-8')).hexdigest()
        linshi = str(IDj)+str(IDucp)
        UIDj = md5(linshi.encode('utf-8')).hexdigest()
        PTCj = str_xor(TCj,UIDj)
        print('IDj:'+IDj)
        IDj_haha = IDj
        print('PTCJ:'+PTCj)
        print('Rj:'+Rj)
        print('TCj:'+TCj)
        TS2 = time.time()
        print(bytes(IDj.encode('utf-8')))
        s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        s.connect(('47.108.195.179',34126))
        s.send(bytes(IDj.encode('utf-8')))
        print(IDj.encode('utf-8'))
        IDj=s.recv(1024)
        testhaha = ''
        for i in IDj:
            testhaha += '%02x' % i
        s.close()
        print(testhaha)
        PIDj = testhaha
        s.close()
        print('-------------------start------------------')
        print('Rj:'+Rj)
        print('IDucp:'+IDucp)
        PRj = str_xor(Rj,IDucp)
        print('PRj:'+PRj)
        print('-------------------end--------------------')
        mySEN = sen(PIDj = PIDj,PRj = PRj)
        mySEN.save()
        post_data = {'PTCj': PTCj,'TS2':TS2}
        if IDj_haha == '201f114d70e54c5b6764aa0d6bb62a05':
            response = requests.post('http://localhost:8002/register_two', data=post_data)
            return HttpResponse(response)
        elif IDj_haha == '36b876925d28488346d08b38f7513d89':
            response = requests.post('http://localhost:8003/register_two', data=post_data) 
            return HttpResponse(response)
        else:
            return HttpResponse('fail')


#登录阶段
            
def user_login(request):
    EIDi = eval(request.POST.get('EIDi'))
    TS1 = float(request.POST.get('TS1'))
    TS_now = time.time()
    if (TS_now-TS1)<15:
        back_text = des_descrypt(EIDi)
        res = eval(back_text.decode('utf-8'))
        UIDi = res["UIDi"]
        IDj = res["IDj"]
        global IDj_haha
        IDj_haha = IDj
        Ni = res["Ni"]
        global Ni_haha
        Ni_haha = Ni
        TS1_decode = res["TS1"]
        if (TS1 == TS1_decode):
            s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            s.connect(('47.108.195.179',34126))
            s.send(bytes(UIDi.encode('utf-8')))
            print(UIDi.encode('utf-8'))
            PIDi=s.recv(1024)
            testhaha = ''
            for i in PIDi:
                testhaha += '%02x' % i
            s.close()
            print(testhaha)
            PIDi = testhaha
            print(PIDi)
            s.close()
            isRegister = gateway.objects.filter(PIDi = PIDi).exists()
            if isRegister:
                V = gateway.objects.get(PIDi = PIDi).V
                linshi = str(IDucp)+str(PIDi)
                V_s = md5(linshi.encode('utf-8')).hexdigest()
                if (V == V_s):
                    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
                    s.connect(('47.108.195.179',34126))
                    s.send(bytes(IDj.encode('utf-8')))
                    print('aaaaaaaaaaaa')
                    print(IDj.encode('utf-8'))
                    PIDj=s.recv(1024)
                    testhaha = ''
                    for i in PIDj:
                        testhaha += '%02x' % i
                    s.close()
                    PIDj = testhaha
                    s.close()
                    PRj = sen.objects.get(PIDj = PIDj).PRj
                    Rj = str_xor(PRj,IDucp)
                    linshi = str(IDj)+str(Rj)
                    TCj = md5(linshi.encode('utf-8')).hexdigest()
                    global TCj_haha
                    TCj_haha = TCj
                    print('Rj:'+Rj)
                    linshi = str(IDj)+str(IDucp)
                    UIDj = md5(linshi.encode('utf-8')).hexdigest()
                    global UIDj_haha
                    UIDj_haha = UIDj
                    TS2 = time.time()
                    linshi = str(TCj)+str(IDj)+str(TS2)
                    TRN1 = str_xor(Ni,md5(linshi.encode('utf-8')).hexdigest())
                    first_test = {'TRN1':TRN1,'TCj':TCj,'UIDj':UIDj,'TS2':TS2}
                    se_test = json.dumps(first_test)
                    print(se_test)
                    mac = hmac.new(bytes(Ni.encode("utf8")),bytes(se_test.encode("utf8")),md5)
                    print('Ni:'+Ni)
                    q1 = mac.hexdigest()
                    post_data = {'UIDj': UIDj,'TRN1':TRN1,'q1':q1,'TS2':TS2}
                    print('aaaaaaaaab'+q1)
                    if IDj == '201f114d70e54c5b6764aa0d6bb62a05':
                        response = requests.post('http://localhost:8002/login', data=post_data)
                    elif IDj == '36b876925d28488346d08b38f7513d89':
                        response = requests.post('http://localhost:8003/login', data=post_data)
                    else:
                        response = 'fp_error'
                    return HttpResponse(response)
                else:
                    return HttpResponse('password_error')
            else:
                return HttpResponse('not_register')
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')
        
    

def login_two(request):
    TRN2 = request.POST.get('TRN2')
    q2 = request.POST.get('q2')
    TS3 = float(request.POST.get('TS3'))
    TS_now = time.time()
    if (TS_now-TS3)<15:
        linshi = str(Ni_haha)+str(TCj_haha)+str(IDj_haha)+str(TS3)
        Nj = str_xor(TRN2,md5(linshi.encode('utf-8')).hexdigest())
        first_test = {'TRN2':TRN2,'Ni':Ni_haha,'TCj':TCj_haha,'UIDj':UIDj_haha,'TS3':TS3}
        se_test = json.dumps(first_test)
        mac = hmac.new(bytes(Nj.encode("utf8")),bytes(se_test.encode("utf8")),md5)
        q2_s = mac.hexdigest()
        print('============================')
        print(first_test)
        print('q2_s:'+q2_s)
        print('q2:'+q2)
        print('Ni:'+Ni_haha)
        print('Nj:'+Nj)
        print('============================')
        if q2 == q2_s:
            print('=====================yes==================')
            TS4 = time.time()
            first_test = {"Nj":Nj,"TS4":TS4}
            se_test = json.dumps(first_test)
            ENj = des_encrypt(se_test)
            post_data = {'ENj': str(ENj),'TS4':TS4}
            response = requests.post('http://localhost:8000/login_two', data=post_data)
            return HttpResponse(response)
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')
"""
def user_register(request):
    TS1 = request.POST.get('TS1')
    ri = request.POST.get('ri')
    IDsc = request.POST.get('IDsc')
    RPWi = request.POST.get('RPWi')
    TS_now = time.time()
    if (TS_now-TS1)<9:
        IDgwn = '0111010101010111010111011101000'
        Ku = '10001111001100110000111010101001'
        DKi = bin(random.randint(0, 9999999999999999999999999999))[2:]
        TCi = md5(str_or(DKi,ri)).encode('utf-8')).hexdigest()
        Bi = md5(str_or(IDsc,RPWi)).encode('utf-8')).hexdigest()
        PTCi = str_xor(TCi,md5(str_or(ri,IDsc)).encode('utf-8')).hexdigest())
        PDKi = str_xor(DKi,ri)
        BNi = str_xor(ri,md5(str_or(str_or（PTCi,IDgwn),Ku)).encode('utf-8')).hexdigest())
        TS2 = time.time()
        post_data = {'Bi': Bi,'PTCi':PTCi,'TS2':TS2}
        response = requests.post('http://localhost:8000/register_receive', data=post_data)
        myGWN = gateway(PTCi = PTCi,PDKi = PDKi,BNi = BNi)
        myGWN.save()

def sen_register(request):
    TS3 = request.POST.get('TS3')
    IDj = request.POST.get('IDj')
    TS_now = time.time()
    if (TS_now-TS3)<9:
        IDgwn = '0111010101010111010111011101000'
        rj = bin(random.randint(0, 9999999999999999999999999999))[2:]
        DKj = bin(random.randint(0, 9999999999999999999999999999))[2:]
        Ksn = '10001111001100110000111010101001'
        TCj = md5(str_or(DKj,rj)).encode('utf-8')).hexdigest()
        PIDj = md5(str_or(DKj,IDj)).encode('utf-8')).hexdigest()
        PTCj = str_xor(TCj,PIDj)
        KIDj = str_xor(IDj,IDgwn)
        PDKj = str_xor(DKj,rj)
        CNj = str_xor(rj,md5(str_or(IDj,str_or(IDgwn,Ksn))).encode('utf-8')).hexdigest())
        TS4 = time.time()
        post_data = {'PTCj': PTCj,'TS4':TS4}
        response = requests.post('http://localhost:8003/register_receive', data=post_data)
        myGWN = gateway(KIDj = KIDj,CNj = CNj,PDKj = PDKj)
        myGWN.save()

def user_login(request):
    TS1 = request.POST.get('TS1')
    PTCi = request.POST.get('PTCi')
    TS_now = time.time()
    if (TS_now-TS1)<9:
        IDgwn = '0111010101010111010111011101000'
        myGate = gateway.objects.get(PTCi = PTCi)
        PDKi = myGate.PDKi
        BNi = myGate.BNi
        Ku = '10001111001100110000111010101001'
        ri = str_xor(BNi,md5(str_or(PTCi,str_or(IDgwn,Ku))).encode('utf-8')).hexdigest())
        DKi = str_xor(PDKi,ri)
        TCi = md5(str_or(DKi,ri)).encode('utf-8')
        Ni = str_xor(PKSi,md5(str_or(TCi,str_or(ri,TS1))).encode('utf-8'))
        IDj = str_xor(PIDj,md5(str_or(TCi,str_or(TS1,Ni))).encode('utf-8'))
        KIDj = str_xor(IDj,IDgwn)
        q1_ = md5(str_or(TCi,str_or(IDj,str_or(Ni,ri)))).encode('utf-8')
        if q1_ == q1:
            myGate2 = gateway.objects.get(KIDj = KIDj)
            PDKj = myGate2.PDKj
            CNj = myGate2.CNj
            Ksn = '10001111001100110000111010101001'
            rj = str_xor(CNj,md5(str_or(IDj,str_or(IDgwn,Ksn))).encode('utf-8')).hexdigest())
            DKj = str_xor(PDKj,rj)
            TCj = md5(str_or(DKj,ri)).encode('utf-8')).hexdigest()
            PIDj = md5(str_or(DKj,IDj)).encode('utf-8')).hexdigest()
            q2 = md5(str_or(str_xor(TCj,Ni),IDj)).encode('utf-8')).hexdigest()
            TS2 = time.time()
            PKSn = str_xor(Ni,md5(str_or(TCj,str_or(IDj,TS2))).encode('utf-8')).hexdigest())
            post_data = {'q2': q2,'PKSn':PKSn,'PIDj':PIDj,'TS2',TS2,'IDj':IDj}
            response = requests.post('http://localhost:8003/login', data=post_data)

def sensor_login_receive(request):
    TS3 = request.POST.get('TS3')
    q3 = request.POST.get('q3')
    PKSj = request.POST.get('PKSj')
    TS_now = time.time()
    if (TS_now-TS3)<9:
        Kj = str_xor(PKSj,md5(str_or(str_or(TCi,Ni),str_or(IDj,TS3))).encode('utf-8')).hexdigest())
        q3_ = md5(str_or(str_or(Ni,str_or(Kj,str_xor(TCj,IDj))))).encode('utf-8')).hexdigest()
        if q3_ == q3:
            PKSk = str_xor(Kj,md5(str_or(TCi,str_xor(Ni,IDj)).encode('utf-8')).hexdigest())
            TS4 = time.time()
            q4 = md5(str_or(TCi,str_or(str_xor(ri,Ni),TS4))).encode('utf-8')).hexdigest()
            post_data = {'q4': q4,'PKSk':PKSk,'TS4',TS4}
            response = requests.post('http://localhost:8000/login_receive', data=post_data)

"""
