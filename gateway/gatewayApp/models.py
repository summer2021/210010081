from django.db import models

# Create your models here.


class gateway(models.Model):
    gid = models.AutoField('编号',primary_key=True)
    PIDi = models.CharField('PIDi',max_length=1000,null=True)
    V = models.CharField('V',max_length=1000,null=True)

    class Meta:
         verbose_name = 'gateway'
         verbose_name_plural = 'gateway'
         ordering = ['gid']

class sen(models.Model):
    sid = models.AutoField('编号',primary_key=True)
    PIDj = models.CharField('PIDj',max_length=1000,null=True)
    PRj = models.CharField('PRj',max_length=2000,null=True)
    
    class Meta:
         verbose_name = 'sen'
         verbose_name_plural = 'sen'
         ordering = ['sid']
