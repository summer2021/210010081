# Generated by Django 3.0.3 on 2021-04-05 15:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='gateway',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False, verbose_name='编号')),
                ('PTCi', models.CharField(max_length=1000, null=True, verbose_name='PTCi')),
                ('PDKi', models.CharField(max_length=1000, null=True, verbose_name='PDKi')),
                ('BNi', models.CharField(max_length=1000, null=True, verbose_name='BNi')),
                ('KIDj', models.CharField(max_length=1000, null=True, verbose_name='KIDj')),
                ('CNj', models.CharField(max_length=1000, null=True, verbose_name='CNj')),
                ('PDKj', models.CharField(max_length=1000, null=True, verbose_name='PDKj')),
            ],
        ),
    ]
