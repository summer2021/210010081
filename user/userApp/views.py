from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from userBackApp.models import user
import json
import simplejson
import requests

import random
import time
import datetime
from hashlib import md5

from Crypto import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as PKCS1_cipher
import base64
import rsa
from pyDes import *

import binascii
import hmac

from Crypto.Util.number import long_to_bytes,bytes_to_long

from pysmx.SM2 import *

# Create your views here.

#white_list = ['127.0.0.1']
len_para = 64

#基本操作





#通信建立阶段


def build_user_one():
    global r1
    r1 = str(bin(random.randint(0, 99999999999999999999))[2:])
    TS1 = time.time()
    post_data = {'r1': r1,'TS1':TS1}
    response = requests.post('http://localhost:8001/build_ucp_one', data=post_data)
    return response


def get_key(key_file):
    with open(key_file ,'rb') as f:
        data = f.read()
    return data

def build_user_two(request):
    TS2_str = request.POST.get('TS2')
    r2 = request.POST.get('r2')
    Kp = request.POST.get('Kp')
    TS2 = float(TS2_str)
    TS_now = time.time()
    if (TS_now-TS2)<15:
        r3 = str(bin(random.randint(0, 99999999999999999999))[2:])
        TS3 = time.time()
        with open('sm2_gate_public_key.pem', 'wb')as f:
            f.write(eval(Kp))
        first_test = {'r3':r3,'TS3':TS3}
        se_test = json.dumps(first_test)
        ER = Encrypt(bytes(se_test.encode('utf-8')), get_key('sm2_gate_public_key.pem'), len_para, 0)
        global SK
        linshi = str(r1)+str(r2)+str(r3)
        SK = md5(linshi.encode('utf-8')).hexdigest()
        print(SK)
        post_data = {'ER': str(ER),'TS3':TS3}
        response = requests.post('http://localhost:8001/build_ucp_two', data=post_data)
        return HttpResponse(response)
    else:
        return HttpResponse('fail')

#注册阶段

#DES
def des_encrypt(s):
    """
    DES 加密
    :param s: 原始字符串
    :return: 加密后字符串，16进制
    """
    secret_key = SK[0:8]
    iv = secret_key
    k = des(secret_key, CBC, iv, pad=None, padmode=PAD_PKCS5)
    en = k.encrypt(s, padmode=PAD_PKCS5)
    return binascii.b2a_hex(en)
 
 
def des_descrypt(s):
    """
    DES 解密
    :param s: 加密后的字符串，16进制
    :return:  解密后的字符串
    """
    secret_key = SK[0:8]
    iv = secret_key
    k = des(secret_key, CBC, iv, pad=None, padmode=PAD_PKCS5)
    de = k.decrypt(binascii.a2b_hex(s), padmode=PAD_PKCS5)
    return de
        
def register_one(request):
    build_user_one()
    IDi = request.POST.get('IDi')
    PWi = request.POST.get('PWi')
    linshi = str(IDi)+str(PWi)
    UIDi = md5(linshi.encode('utf-8')).hexdigest()
    print('------------------')
    print('UIDi:'+UIDi)
    print('------------------')
    TS1 = time.time()
    first_test = {'UIDi':UIDi,'TS1':TS1}
    se_test = json.dumps(first_test)
    EIDi=des_encrypt(se_test)
    post_data = {'EIDi': str(EIDi),'TS1':TS1}
    response = requests.post('http://localhost:8001/user_register', data=post_data)
    return HttpResponse(response)

def register_two(request):
    ACKucp = eval(request.POST.get('ACKucp'))
    TS2_str = request.POST.get('TS2')
    TS2 = float(TS2_str)
    TS_now = time.time()
    if (TS_now-TS2)<15:
        back_text = des_descrypt(ACKucp)
        res = eval(back_text.decode('utf-8'))
        TS2_decode = res["TS2"]
        if (TS2 == TS2_decode):
            return HttpResponse('success')
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')
            


#登录阶段
def login_one(request):
    res = build_user_one()
    print(res.content)
    if(str(res.content)[2:-1]) == "fail":
        return HttpResponse("build_fail")
    IDi = request.POST.get('IDi')
    IDj = request.POST.get('IDj')
    PWi = request.POST.get('PWi')
    print(request.POST.get('IDi'))
    print(PWi)
    Ni = bin(random.randint(0, 99999999999999999999))[2:]
    global Ni_haha
    Ni_haha = Ni
    TS1 = time.time()
    linshi = str(IDi)+str(PWi)
    UIDi = md5(linshi.encode('utf-8')).hexdigest()
    first_test = {'UIDi':UIDi,'IDj':IDj,'Ni':Ni,'TS1':TS1}
    se_test = json.dumps(first_test)
    EIDi = des_encrypt(se_test)
    post_data = {'EIDi': str(EIDi),'TS1':TS1}
    response = requests.post('http://localhost:8001/user_login', data=post_data)
    return HttpResponse(response)

def login_two(request):
    ENj = eval(request.POST.get('ENj'))
    TS4 = float(request.POST.get('TS4'))
    TS_now = time.time()
    if (TS_now-TS4)<15:
        back_text = des_descrypt(ENj)
        res = eval(back_text.decode('utf-8'))
        Nj = res["Nj"]
        TS4_decode = res["TS4"]
        if (TS4 == TS4_decode):
            linshi = str(Ni_haha)+str(Nj)
            KEYij = md5(linshi.encode('utf-8')).hexdigest()
            print('---------------------')
            print('KEYij:'+KEYij)
            print('---------------------')
            return HttpResponse('success')
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')
    
"""

def register_post(request):
    IDsc = str2bin(request.POST.get('IDsc'))
    IDi = str2bin(request.POST.get('IDi'))
    PWi = str2bin(request.POST.get('PWi'))
    a = bin(random.randint(0, 9999999999999999999999999999))[2:]
    global TS1
    TS1 = time.time()
    first = str2bin(md5(str_or(str_or(IDsc,IDi),PWi).encode('utf-8')).hexdigest())
    global ANi
    ANi = str_xor(ri,first)
    RPWi = md5(str_or(str_or(IDsc,ri),PWi).encode('utf-8')).hexdigest()
    post_data = {'IDsc': IDsc,'ri':ri,'RPWi':RPWi,'TS1':TS1}
    response = requests.post('http://localhost:8001/user_register', data=post_data)
    
def register_receive(request):
    TS2 = request.POST.get('TS2')
    Bi = request.POST.get('Bi')
    PCTi = request.POST.get('PCTi')
    if (TS2-TS1)<9:
        hex_ = md5(ANi+Bi+PTCi).hexdigest()
        myUser = user(ANi = ANi,Bi = Bi,PTCi = PTCi,h = hex_)
        myUser.save()

def login(request):
    IDsc = str2bin(request.POST.get('IDsc'))
    IDi = str2bin(request.POST.get('IDi'))
    IDj = str2bin(request.POST.get('IDj'))
    PWi = str2bin(request.POST.get('PWi'))
    myUser = user.objects.get(IDsc = IDsc,IDi = IDi)
    ANi = myUser.ANi
    Bi = myUser.Bi
    ri = str_xor(ANi,md5(str_or(str_or(IDsc,IDi),PWi).encode('utf-8')).hexdigest())
    RPWi = md5(str_or(str_or(IDsc,ri),PWi).encode('utf-8')).hexdigest()
    Bi_ = md5(str_or(IDsc,RPWi).encode('utf-8')).hexdigest()
    if Bi_ == Bi:
        Ni = bin(random.randint(0, 9999999999999999999999999999))[2:]
        TS1 = time.time()
        TCi = str_xor(PTCi,md5(str_or(ri,IDsc).encode('utf-8')).hexdigest())
        q1 = md5(str_or(TCi,str_or(IDj,str_or(Ni,ri))).encode('utf-8')).hexdigest()
        PKSi = str_xor(Ni,md5(str_or(TCi,str_or(ri,TS1)).encode('utf-8')).hexdigest())
        PIDj = str_xor(IDj,md5(str_or(TCi,str_or(TS1,Ni)).encode('utf-8')).hexdigest())
        post_data = {'q1': q1,'PKSi':PKSi,'PIDj':PIDj,'PTCi':PTCi,'TS1':TS1}
        response = requests.post('http://localhost:8001/user_login', data=post_data)
        
def login_receive(request):
    TS4 = request.POST.get('TS4')
    q4 = request.POST.get('q4')
    PKSk = request.POST.get('PKSk')
    TS_now = time.time()
    if (TS_now-TS4)<9:
        q4_ = md5(str_or(str_or(TCi,str_xor(ri,Ni)),TS4).encode('utf-8')).hexdigest()
        if q4_ == q4:
            Kj = str_xor(PKSk,md5(str_or(TCi,str_xor(Ni,IDj)).encode('utf-8')).hexdigest())
            KEYij = md5(str_or(Ni,Kj),TS4).encode('utf-8')).hexdigest()
    
    
"""

