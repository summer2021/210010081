# Generated by Django 3.0.3 on 2021-04-05 13:47

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='user',
            fields=[
                ('uid', models.AutoField(primary_key=True, serialize=False, verbose_name='用户编号')),
                ('an', models.CharField(max_length=1000, null=True, verbose_name='ANi')),
                ('b', models.CharField(max_length=1000, null=True, verbose_name='Bi')),
                ('ptc', models.CharField(max_length=1000, null=True, verbose_name='PTC')),
                ('h', models.CharField(max_length=1000, null=True, verbose_name='h(·)')),
            ],
        ),
    ]
