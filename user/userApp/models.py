from django.db import models

# Create your models here.

class user(models.Model):
    uid = models.AutoField('用户编号',primary_key=True)
    #根据协议，用户端的数据库不需要存储任何东西

class Meta:
     verbose_name = 'user'
     verbose_name_plural = 'user'
     ordering = ['uid']
