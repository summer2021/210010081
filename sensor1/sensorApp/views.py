from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from sensorBackApp.models import sensor
import json
import simplejson

import time
import datetime
import random
from hashlib import md5
import requests

import hmac
from Crypto.Util.number import long_to_bytes,bytes_to_long

IDj = '201f114d70e54c5b6764aa0d6bb62a05'

# Create your views here.


#基本操作


def str_xor(s1, s2):
  s = ""#结果字符串
  l1 = len(s1)
  l2 = len(s2)
  print(l1, l2)
  for i in range(0, min(l1, l2)):#首先将两个字符串中本来存在的数据进行异或
    # print(s1[i], s2[i])
    a = ord(s1[i])#字符串转十进制
    b = ord(s2[i])
    s = s + chr(a^b)#将异或后结果转字符存到结果字符串中
  # print(s)
  for i in range(min(l1,l2),max(l1,l2)):#将某一字符串多出来的部分进行异或操作
    if(l1<l2):
      s = s + chr(ord(s2[i])^255)
    else :
      s = s + chr(ord(s1[i])^255)#和11111111进行异或
  return s

#注册阶段
def register_one(request):
    TS1 = time.time()
    post_data = {'IDj': IDj,'TS1':TS1}
    requests.post('http://localhost:8001/sensor_register', data=post_data)
    return HttpResponse('finish')

def register_two(request):
    TS2 = eval(request.POST.get('TS2'))
    PTCj = request.POST.get('PTCj')
    TS_now = time.time()
    if (TS_now-TS2)<15:
        mySen = sensor(PTCj = PTCj)
        mySen.save()
        return HttpResponse('success')


#登录阶段
def login(request):
    UIDj = request.POST.get('UIDj')
    TRN1 = request.POST.get('TRN1')
    q1 = request.POST.get('q1')
    TS2 = float(request.POST.get('TS2'))
    TS_now = time.time()
    if (TS_now-TS2)<15:
        print('wwwwwwwwwwwwwwww')
        PTCj = sensor.objects.get().PTCj
        TCj = str_xor(PTCj,UIDj)
        print('PTCJ:'+PTCj)
        print('TCTCJ:'+md5(TCj.encode('utf-8')).hexdigest())
        linshi = str(TCj)+str(IDj)+str(TS2)
        Ni = str_xor(TRN1,md5(linshi.encode('utf-8')).hexdigest())
        first_test = {'TRN1':TRN1,'TCj':TCj,'UIDj':UIDj,'TS2':TS2}
        se_test = json.dumps(first_test)
        print(se_test)
        mac = hmac.new(bytes(Ni.encode("utf8")),bytes(se_test.encode("utf8")),md5)
        q1_s = mac.hexdigest()
        print('======================')
        print('Ni:'+Ni)
        print('q1_s:'+q1_s)
        print('q1:'+q1)
        print('======================')
        if q1 == q1_s:
            print('wuwuwuwwuwu')
            Nj = bin(random.randint(0, 9999999999999999999999999999))[2:]
            TS3 = time.time()
            linshi = str(Ni)+str(TCj)+str(IDj)+str(TS3)
            TRN2 = str_xor(Nj,md5(linshi.encode('utf-8')).hexdigest())
            first_test1 = {'TRN2':TRN2,'Ni':Ni,'TCj':TCj,'UIDj':UIDj,'TS3':TS3}
            se_test1 = json.dumps(first_test1)
            mac1 = hmac.new(bytes(Nj.encode("utf8")),bytes(se_test1.encode("utf8")),md5)
            q2 = mac1.hexdigest()
            linshi = str(Ni)+str(Nj)
            KEYij = md5(linshi.encode('utf-8')).hexdigest()
            print('---------------------')
            print('KEYij:'+KEYij)
            print(se_test1)
            print('q2:'+q2)
            print('Ni:'+Ni)
            print('Nj:'+Nj)
            print('---------------------')
            post_data = {'TRN2': str(TRN2),'q2':q2,'TS3':TS3}
            response = requests.post('http://localhost:8001/login_two', data=post_data)
            return HttpResponse(response)
        else:
            return HttpResponse('fail')
    else:
        return HttpResponse('fail')

"""
def register(request):
    IDj = '0111010101010111010111011101000'
    TS3 = time.time()
    post_data = {'IDj': IDj,'TS3':TS3}
    response = requests.post('http://localhost:8001/sensor_register', data=post_data)

def register_receive(request):
    TS4 = request.POST.get('TS4')
    TS_now = time.time()
    if (TS_now-TS4)<9:
        mySen = sensor(PTCj = PTCj)
        mySen.save()

def login(request):
    TS2 = request.POST.get('TS2')
    IDj = request.POST.get('IDj')
    q2 = request.POST.get('q2')
    TS_now = time.time()
    if (TS_now-TS2)<9:
        TCj = str_xor(PTCj,PIDj)
        Ni = str_xor(PKSn,md5(str_or(str_or(TCj,IDj),TS2)).encode('utf-8')).hexdigest())
        q2_ = md5(str_or(str_xor(TCj,Ni),IDj)).encode('utf-8')).hexdigest()
        if q2 == q2_:
            Kj = bin(random.randint(0, 9999999999999999999999999999))[2:]
            q3 = md5(str_or(str_or(Ni,Ki),str_xor(TCj,IDj))).encode('utf-8')).hexdigest()
            TS3 = time.time()
            PKSj = str_xor(Kj,md5(str_or(str_or(TCj,Ni),str_xor(IDj,TS3))).encode('utf-8')).hexdigest())
            KEYij = md5(str_or(Ni,Kj)).encode('utf-8')).hexdigest()
            post_data = {'q3': q3,'PKSj':PKSj,'TS3':TS3}
            response = requests.post('http://localhost:8001/sensor_login_receive', data=post_data)
"""
