from django.apps import AppConfig


class SensorbackappConfig(AppConfig):
    name = 'sensorBackApp'
